# SampleWebApplication

jenkins勉強用Webアプリケーション

# フォーク元プロジェクト
https://github.com/ko-aoki/springboot-doma.git

# マニュアル
http://springboot-domamaster-maintenance-sample.readthedocs.io/ja/latest/

# 実行
- tomcatにデプロイする
- javaコマンドで実行する
```
java -jar springboot-doma.war
```

# アクセス
http://localhost:8080/edu <br>

プリセットアカウント<br>

|アカウント|パスワード|備考  |
|---------|---------|------|
| 01      |test     |管理者|
| 02      |test     |一般  |
